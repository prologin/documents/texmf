\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{prologin-letter}[2014/06/12 Yves Stadler, v1.0]

\newcommand{\lieu}[1]{\def\thelieu{#1}}
\newcommand{\name}[1]{}
\newcommand{\titre}[1]{}
\newcommand{\phone}[1]{}
\newcommand{\email}[1]{}
\makeatletter
\newif\if@personal\@personalfalse
\DeclareOption{personal}{
    \renewcommand{\name}[1]{\def\thename{#1}}
    \renewcommand{\titre}[1]{\def\thetitre{#1}}
    \renewcommand{\phone}[1]{\def\thephone{#1}}
    \renewcommand{\email}[1]{\def\themail{#1}}
    \@personaltrue 
}

\newif\if@signimg\@signimgfalse
\DeclareOption{signimg}{
    \@signimgtrue
}
\makeatother

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrlttr2}} 

\ProcessOptions\relax

%% Load base class
\LoadClass[NF,version=last]{scrlttr2}

%%
%% Configuration option for letters with scrlttr2 (KomaScript)
%%
\RequirePackage{adjustbox}
\KOMAoptions{foldmarks=true,foldmarks=BlmTp, backaddress=false, fromalign=left,
fromphone,fromemail,fromlogo,subject=left}
% Sender information
\makeatletter
\if@personal
    \setkomavar{fromname}{\thename{}, Association Prologin}
\else
    \setkomavar{fromname}{Association Prologin}
    \def\thephone{+33 144 080 190}
    \def\themail{info@prologin.org}
\fi
\makeatother

    \renewcommand*{\raggedsignature}{\raggedright}
    \setkomavar{fromaddress}{14-16 rue Voltaire\\
    94270 le Kremlin-Bic\^{e}tre}
    \setkomavar{fromlogo}{%
    \adjustbox{valign=B,raise=12mm}{%
    \includegraphics{logo\editionyear}}}
% Member information
    \makeatletter
        \if@personal
        \setkomavar{signature}{Pour l'association Prologin,\\%
            \thetitre\\%
            \thename{}\\%
            \if@signimg%
            \includegraphics[width=.35\textwidth]{sign.png}%
            \else{}\fi%
        }
        \else
        \setkomavar{signature}{Prologin}
        \fi
        % Set by macro above
        \setkomavar{fromemail}{\themail}
        \setkomavar{fromphone}{\thephone}
    \makeatother
    %\setkomavar{location}{\raggedright
    %Yves Stadler\\
    %Responsable communication}

\makeatletter
\@setplength{sigindent}{.60\textwidth}
\makeatother

% vim: set ft=tex :


%% Configuration for Prologin letters
\RequirePackage{graphics}
\RequirePackage{scrdate}
\RequirePackage{prologin-std}

%% Set the date format at the start of the document
\AtBeginDocument{\setkomavar{date}{À \thelieu, le \MakeLowercase{\todaysname} \today}}

\setlength{\parskip}{10pt}
%% vim: set ft=tex:

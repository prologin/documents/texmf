% Original Author: Yves `M'vy` Stadler

\NeedsTeXFormat{LaTeX2e}
%% Name of the class
\ProvidesClass{prologin}[Prologin LaTeX class v1.1]
%% Options
% noheader allows to mask header
\DeclareOption{noheader}{
    \PassOptionsToPackage{\CurrentOption}{fancyhdr-std}
}

% nofooter allows to mask footer
\DeclareOption{nofooter}{
    \PassOptionsToPackage{\CurrentOption}{fancyhdr-std}
}

% samehead disables the front page special headers and footers
\DeclareOption{samehead}{
    \PassOptionsToPackage{\CurrentOption}{fancyhdr-std}
}

% nodate disables the date in headers
\DeclareOption{nodate}{
    \PassOptionsToPackage{\CurrentOption}{fancydr-std}
}

% Page count option
\DeclareOption{pagecount}{
    \PassOptionsToPackage{\CurrentOption}{fancyhdr-std}
}

\DeclareOption{landscape}{
    \PassOptionsToPackage{\CurrentOption}{fancyhdr-std}
    \PassOptionsToPackage{\CurrentOption}{article}
}

% If `english' option is set, the default language will be english, otherwise it's french
\DeclareOption{english}{
    \PassOptionsToPackage{main=english}{babel}
}

% samehead disables the front page special headers and footers
\makeatletter
\newif\if@notitle\@notitlefalse
\DeclareOption{notitle}{
    \@notitletrue
}
\makeatother

%% Base class option
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax

%% Package
\LoadClass{article}
\RequirePackage{prologin-std}
%% Default pagestyle + last page management
\RequirePackage{fancyhdr,lastpage}
\usepackage{fancyhdr-std}

%% Loading graphics for logo
\RequirePackage{graphicx}
\graphicspath{{images/}}

%% Macros
\makeatletter
\def\@subtitle{}
\newcommand{\subtitle}[1]{\def\@subtitle{#1\\}}
\makeatother

\newcommand\dotspace[1]{\parbox[b]{#1}{\dotfill}}

%% New title style
\makeatletter
\if@notitle\def\maketitle{}\else
\def\maketitle{%
    \vspace{1cm}
\begin{center}%
\begin{minipage}{.95\linewidth}
    \center\LARGE\textbf{\@title}\\
    \large\@subtitle
    \large\@author
\end{minipage}
\end{center}%
\vspace{1cm}%
}
\fi
\makeatother

%% First page style

\endinput
% vim: set ft=tex :
